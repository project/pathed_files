<?php

/**
 * @file
 * Basic hooks for pathed_files.
 */

/**
 * Implements hook_menu().
 */
function pathed_files_menu() {
  $items['admin/content/file/pathed-files'] = array(
    'title' => 'Pathed files',
    'description' => 'Manage your pathed files.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathed_files_admin_list'),
    'access arguments' => array('administer pathed_files'),
    'file' => 'pathed_files.admin.inc',
    'type' => MENU_NORMAL_ITEM,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/file/pathed-files/list'] = array(
    'title' => 'Pathed files',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/content/file/pathed-files/%'] = array(
    'title' => 'Edit',
    'description' => 'Edit file',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathed_files_admin_edit', 4),
    'access arguments' => array('administer pathed_files'),
    'file' => 'pathed_files.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/content/file/pathed-files/%/edit'] = array(
    'title' => 'Edit',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/content/file/pathed-files/%/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete file',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathed_files_admin_delete', 4),
    'access arguments' => array('administer pathed_files'),
    'file' => 'pathed_files.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/file/pathed-files/new'] = array(
    'title' => 'New pathed file',
    'description' => 'New pathed file',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathed_files_admin_edit'),
    'access arguments' => array('administer pathed_files'),
    'file' => 'pathed_files.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  // Generate the Pathed Files routes.
  foreach (_pathed_files_get_list(TRUE) as $pathed_file) {
    $uri = trim($pathed_file->file_uri_path);
    $uri = trim($uri, '/');

    $items[$uri] = array(
      'page callback' => 'pathed_files_handler',
      'page arguments' => array($pathed_file->id),
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    );
  }

  if (module_exists('raw_pages')) {
    $items['admin/content/file/pathed-files/migrate'] = array(
      'title' => 'Migrate raw pages',
      'description' => 'Migrate pages configured by raw_pages module.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('pathed_files_migrate_raw_pages'),
      'access callback' => 'pathed_files_migrate_user_access',
      'file' => 'pathed_files.migrate.inc',
      'type' => MENU_LOCAL_TASK,
    );
  }

  return $items;
}

/**
 * Determines whether user can administer both pathed_files and raw_pages.
 */
function pathed_files_migrate_user_access() {
  return user_access('administer pathed_files') && user_access('administer raw pages');
}

/**
 * Implements hook_permission().
 */
function pathed_files_permission() {
  return array(
    'administer pathed_files' => array(
      'title' => t('Administer pathed files'),
      'description' => t('Create and edit pathed files.'),
    ),
  );
}

/**
 * Gets the list of pathed files from the database.
 *
 * @param bool $enabled_only_flag
 *   TRUE to only show enabled files.
 *
 * @return resource
 *   Query object.
 */
function _pathed_files_get_list($enabled_only_flag = TRUE) {
  if ($enabled_only_flag) {
    return db_query('SELECT * FROM {pathed_files} WHERE enabled_flag = 1 ORDER BY id');
  }

  return db_query('SELECT * FROM {pathed_files} ORDER BY id');
}

/**
 * Get a single pathed_files entry from ID.
 */
function _pathed_files_get_entry($id) {
  static $found = array();
  if (isset($found[$id])) {
    return $found[$id];
  }

  $result = db_query('SELECT * FROM {pathed_files} WHERE id = :id', array(':id' => $id));
  $found[$id] = $result->fetchObject();

  return $found[$id];
}

/**
 * Returns the file to the browser.
 */
function pathed_files_handler($pathed_file_id) {
  $pathed_file = _pathed_files_get_entry($pathed_file_id);
  if (empty($pathed_file)) {
    drupal_set_message(t('Invalid pathed file %id', array('%id' => $pathed_file->id)));
    return MENU_NOT_FOUND;
  }

  if (!empty($pathed_file->use_file_content)) {
    _pathed_files_set_file_headers($pathed_file);
    print $pathed_file->file_content;
    // The next two calls ensure page caching is respected.
    drupal_page_footer();
    exit();
  }
  else {
    $uploaded_file = file_load($pathed_file->file_id);
    file_transfer($uploaded_file->uri, file_get_content_headers($uploaded_file));
  }
}

/**
 * Set headers for common file types.
 *
 * @todo Allow other content types besides plain and XML.
 */
function _pathed_files_set_file_headers($file) {
  drupal_add_http_header('Content-Length', strlen($file->file_content));
  if (!preg_match('#\.(.*)$#', $file->file_uri_path, $matches)) {
    // Grab the file extension to determine content type.
    return;
  }

  $extension = $matches[1];
  module_load_include('inc', 'pathed_files', 'includes/mime_types_map');
  $map = _pathed_files_get_mime_types();

  if (isset($map[$extension])) {
    drupal_add_http_header('Content-type', $map[$extension]);
  }
}
