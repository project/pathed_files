== Introduction ==

The Pathed Files module allows content editors to manage miscellaneous
files that can be accessible from any path in the website. For example, if you
need a file at www.example.com/verify.xml, instead of uploading the verify.xml
file to your site root, you can create a "pathed file" in via the Drupal CMS.

To create a file, you define its path (as well as a name and description for
internal organization) and whether you want to upload the file (for a binary
file like image or PDF) or manually enter its contents (for a plain text file).

== Installation ==

Install the module as normal and configure permissions.

== Configuration ==

Go to the Pathed Files homepage: admin/content/file/pathed-files. You can manage
files here.
